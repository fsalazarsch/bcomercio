from django.apps import AppConfig


class BolsaSantiagoConfig(AppConfig):
    name = 'bolsa_santiago'
