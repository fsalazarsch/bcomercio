from django.db import models
import datetime, json
from pathlib import Path
from json import JSONDecodeError
from django.contrib import admin
from django.contrib.auth.models import User

# Create your models here.
class moneda(models.Model):
    id = models.CharField(max_length=5, primary_key=True)
    nombre = models.CharField(max_length=30)
    def __str__(self):
        return self.nombre
class monedaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']
admin.site.register(moneda, monedaAdmin)

class accion(models.Model):
    nemotecnico = models.CharField(max_length=20, null=True, blank=True)
    isin = models.CharField(max_length=20, null=True, blank=True)
    ultimo = models.FloatField(blank=True, null=True)
    proc_variacion = models.FloatField(blank=True, null=True)
    volumen = models.BigIntegerField(blank=True, null=True)
    monto = models.BigIntegerField(blank=True, null=True)
    compra = models.FloatField(blank=True, null=True)
    venta = models.FloatField(blank=True, null=True)
    moneda = models.ForeignKey(moneda, blank=True, null=True, on_delete=models.SET_NULL)
    accion_extranjera = models.BooleanField(default=False)
    etfs_extranjera = models.BooleanField(default=False)
    igpa = models.BooleanField(default=False)
    ipsa = models.BooleanField(default=False)
    fechahora = models.DateTimeField(auto_now=True)
    class Meta:
        unique_together = ('nemotecnico', 'fechahora')
class accionAdmin(admin.ModelAdmin):
    list_display = ['nemotecnico', 'isin', 'ultimo', 'proc_variacion', 'volumen', 'monto', 'compra', 'venta', 'moneda', 'accion_extranjera', 'etfs_extranjera', 'ipsa', 'igpa', 'fechahora']
admin.site.register(accion, accionAdmin)

class valorMonetario(models.Model):
    monetario = models.CharField(max_length=20, primary_key=True)
    precio = models.FloatField(blank=True, null=True)
    proc_variacion = models.FloatField(blank=True, null=True)
    volumen = models.BigIntegerField(blank=True, null=True)
    monto = models.FloatField(blank=True, null=True)
    compra = models.FloatField(blank=True, null=True)
    venta = models.FloatField(blank=True, null=True)
    fechahora = models.DateTimeField(auto_now=True)
class valorMonetarioAdmin(admin.ModelAdmin):
    list_display = ['monetario', 'precio', 'proc_variacion', 'volumen', 'monto', 'compra', 'venta', 'fechahora']
admin.site.register(valorMonetario, valorMonetarioAdmin)

class benchmark_rf(models.Model):
    benchmarkrf = models.CharField(max_length=20, primary_key=True)
    valor = models.FloatField(blank=True, null=True)
    proc_variacion = models.FloatField(blank=True, null=True)	
    fechahora = models.DateTimeField(auto_now=True)
class benchmark_rfAdmin(admin.ModelAdmin):
    list_display = ['benchmarkrf', 'valor', 'proc_variacion', 'fechahora']
admin.site.register(benchmark_rf, benchmark_rfAdmin)

class indice_rf(models.Model):
    indicerf = models.CharField(max_length=20, primary_key=True)
    valor = models.FloatField(blank=True, null=True)
    proc_variacion = models.FloatField(blank=True, null=True)	
    fechahora = models.DateTimeField(auto_now=True)
class indice_rfAdmin(admin.ModelAdmin):
    list_display = ['indicerf', 'valor', 'proc_variacion', 'fechahora']
admin.site.register(indice_rf, indice_rfAdmin)

class instrumento_rf(models.Model):
    instrumentorf = models.CharField(max_length=30, primary_key=True)
    monto = models.BigIntegerField(blank=True, null=True)
    nro_negocio = models.IntegerField(blank=True, null=True)
    fechahora = models.DateTimeField(auto_now=True)
class instrumento_rfAdmin(admin.ModelAdmin):
    list_display = ['instrumentorf', 'monto', 'nro_negocio', 'fechahora']
admin.site.register(instrumento_rf, instrumento_rfAdmin)

class tasa_mercado_rf(models.Model):
    duracion = models.CharField(max_length=100, primary_key=True)
    tasas = models.TextField(null=True, blank=True)
    fechahora = models.DateTimeField(auto_now=True)
class tasa_mercado_rfAdmin(admin.ModelAdmin):
    list_display = ['duracion', 'fechahora']
admin.site.register(tasa_mercado_rf, tasa_mercado_rfAdmin)

