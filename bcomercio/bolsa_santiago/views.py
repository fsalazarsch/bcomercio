
import uuid, csv

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.defaults import page_not_found

#from acafi.models import administrador, fondo, fondo_ext, categoria, perfil, bug

#from acafi.deploy import escribir_log


# Create your views here.

from sendgrid.helpers.mail import *

#Envia un mail usando sendgrid
def enviar_mail(titulo, cuerpo, mail):
    apikey = "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"
    sg = sendgrid.SendGridAPIClient(apikey="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")
    from_email = Email("no-reply@acafi.cl")
    to_email = Email(mail)
    content = Content("text/html", cuerpo)
    mail = Mail(from_email, titulo, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())
    
#crea una contraseña para el usuario
def activar_usuario(request, token): 
    try:
        p = perfil.objects.get(token = token)
        if p.token == '':
            response = '2'
        else:
            u = p.usuario
            u.set_password(u.first_name+u.last_name)
            u.save()
            p.token =''
            p.save()
            response = '1'

            cuerpo = "Felicidades:<br> Ha activado correctamente su usuario: <br>"
            cuerpo +="<b>Credenciales de acceso</b><br>"
            cuerpo += "<b>Nombre:</b> "+str(u.username)+"<br>"
            cuerpo += "<b>Password:</b> "+str(u.first_name+u.last_name)+"<br>"
            cuerpo += "(Le recomendamos cambiar su contraseña)"
            enviar_mail('Sistema ACAFI', cuerpo, u.email)

    except ObjectDoesNotExist:

        response = '2'


    
    return HttpResponseRedirect('/login/?response='+response)


#pagina de perfil (adm y user)
def getperfil(request, response=None):
    if request.user.is_authenticated:
        #escribir_log( "Usuario id:"+str(request.user.id)+" nombre:"+str(request.user.username)+" ingreso" , "secciones/perfil")
        #if request.user.is_superuser or request.user.is_staff:
            #nro_categ = categoria.objects.count()
            #nro_factsheets = fondo.objects.exclude(vigente=False).count()
            #nro_users = User.objects.count()
            #nro_admin = administrador.objects.count()
        lista_usuarios = User.objects.all()
        #lista_fondos = fondo.objects.all()

        #if request.user.is_superuser == 0 and request.user.is_staff == 0:
            #perf = perfil.objects.get(pk=request.user)
            #lista_fondos = lista_fondos.filter(admin=perf.administrador)
            # lista_fondos = lista_fondos.filter(user = request.user)
            #lista_fondos = lista_fondos.filter(vigente = True)

        url = ''
        filter_rut = request.GET.get('filter_rut')
        filter_name = request.GET.get('filter_name')
        filter_name2 = request.GET.get('filter_name2')
        filter_vigente = request.GET.get('filter_vigente')
        filter_anio = request.GET.get('filter_anio')

        if filter_rut != None and filter_rut != '' and filter_rut != 'None':
            lista_fondos = lista_fondos.filter(runsvs__icontains=filter_rut)
            url += 'filter_rut=' + filter_rut + '&'
        else:
            filter_rut = ''

        if filter_name2 != None and filter_name2 != '' and filter_name2 != 'None':
            lista_fondos = lista_fondos.filter(nombre__icontains=filter_name2)
            url += 'filter_name2=' + filter_name2 + '&'
        else:
            filter_name2 = ''

        if filter_name != None and filter_name != '' and filter_name != 'None':
            lista_fondos = lista_fondos.filter(admin_id__razon_social__icontains=filter_name)
            url += 'filter_name=' + filter_name + '&'

        if filter_vigente != None and filter_vigente != '' and filter_vigente != 'None':
            lista_fondos = lista_fondos.filter(vigente=filter_vigente)
            url += 'filter_vigente=' + filter_vigente + '&'

        if filter_anio != None and filter_anio != '' and filter_anio != 'None':
            lista_fondos = lista_fondos.filter(inicio_operaciones__year__lte=filter_anio)
            url += 'filter_anio=' + filter_anio + '&'

        page = request.GET.get('page')
        if page is None:
            page = 1

        #paginator = Paginator(lista_fondos, 15)

        #lista_fondos = lista_fondos[(15 * (int(page) - 1)): 15 * (int(page) - 1) + 15]

        aux = {}


        '''
        try:
            contacts = paginator.page(page)
        except PageNotAnInteger:
            contacts = paginator.page(1)
        except EmptyPage:
            contacts = paginator.page(paginator.num_pages)
		'''
        filtros = {'filter_rut': filter_rut, 'filter_name': filter_name,
                   'filter_name2': filter_name2, 'filter_vigente' : filter_vigente, 'filter_anio': filter_anio, "url" : url}
        if request.user.is_superuser or request.user.is_staff:
            context = {'response': response, 'lista_usuarios': lista_usuarios}
        else:
            context = {'response': response, 'lista_usuarios': lista_usuarios}

        merged_dict = dict(list(filtros.items()) + list(context.items()))

        if request.user.is_superuser or request.user.is_staff:
            return render(request, "index.html", merged_dict)
        else:
            return render(request, "index_user.html", merged_dict)

    else:
        return HttpResponseRedirect("/login/")

#front y back de login
def get_login(request, response=None):
    if request.method == "POST":
        username =request.POST.get('username') 
        passwd = request.POST.get('password')
        user = authenticate(request, username=username, password=passwd)
        if user is not None:
            login(request, user)
            response = 1
            return HttpResponseRedirect('/perfil/')
        else:
            context = {'response': 0}
            return render(request, "login.html", context)

    else:
        context = {'response': response}
        return render(request, "login.html", context)


#recuperar contraseña
def recuperar(request, response=None):
    if request.method == "POST":
        mail_user = request.POST.get('username')
        try:
            u = User.objects.get(email=mail_user)
            token = uuid.uuid4().hex[0:6]
            u.set_password(token)
            u.save()

            cuerpo = "Se ha reiniciando su contraseña: <br>"
            cuerpo += "<b>Password:</b> " + str(token) + "<br>"
            cuerpo += "(Le recomendamos cambiar su contraseña)"
            enviar_mail('Sistema ACAFI', cuerpo, u.email)
            context = {'response': 2}
            return render(request, "recuperar.html", context)

        except ObjectDoesNotExist:
            try:
                u = User.objects.get(username=mail_user)
                token = uuid.uuid4().hex[0:6]
                u.set_password(token)
                u.save()

                cuerpo = "Se ha reiniciando su contraseña: <br>"
                cuerpo += "<b>Password:</b> " + str(token) + "<br>"
                cuerpo += "(Le recomendamos cambiar su contraseña)"
                enviar_mail('Sistema ACAFI', cuerpo, u.email)
                context = {'response': 2}
                return render(request, "recuperar.html", context)

            except ObjectDoesNotExist:
                context = {'response': 1}
                return render(request, "recuperar.html", context)

        return HttpResponse(mail_user)
        # send_mail
    else:
        context = {'response': response}
        return render(request, "recuperar.html", context)


def mi_error_404(request):
    nombre_template = '404.html'
    return page_not_found(request, template_name=nombre_template)
      


