import requests, re, unicodedata, urllib3, datetime, json, time

from django.shortcuts import render
from bs4 import BeautifulSoup, element, SoupStrainer
from bolsa_santiago.models import accion, moneda
#from acafi.deploy import escribir_log
#from acafi.scraping.listar_empresas import get_periodos


def get_acciones():
    i = 1
    u = 'http://www.bolsadesantiago.com/mercado/Paginas/Acciones.aspx?RequestAjax=1&hdnPag='+str(i)+'&hdnNombre=&hdnTipo=TODAS'
    
    r = requests.get(u)
    result = r.json()

    tot = int(result['TotalPaginas'])
    for i in range(1, tot+1):
        u = 'http://www.bolsadesantiago.com/mercado/Paginas/Acciones.aspx?RequestAjax=1&hdnPag='+str(i)+'&hdnNombre=&hdnTipo=TODAS'
        r = requests.get(u)
        result = r.json()

        for acc in result['ListaAcciones']:
            try:
                a = accion.objects.get(nemotecnico = acc['Nemo'], fechahora= time.strftime("%Y-%m-%d %H:%M"))
            except accion.DoesNotExist:
                a = accion(nemotecnico =acc['Nemo'], isin=acc['Isin'], ultimo=acc['PrecioCierre'],\
                    proc_variacion= acc['Variacion'], volumen= acc['Volumen'], monto=acc['Monto'], \
                    compra=acc['Compra'], venta= acc['Venta'], accion_extranjera=acc['enAccionesExtranjeras'], \
                    etfs_extranjera= acc['enETFsExtranjeros'], ipsa=acc['enIPSA'], fechahora= time.strftime("%Y-%m-%d %H:%M"))
                a.save()
                try:
                    m = moneda.objects.get(pk=acc['Moneda'])
                except moneda.DoesNotExist:
                    m = moneda.objects.get(pk='---')
                    
                a.moneda = m
                a.save()


        


